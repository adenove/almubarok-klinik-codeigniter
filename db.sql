alter table rujukan
add column nomorRujukan varchar(100);

alter table dokter
add column spesialis varchar(100);

alter table rujukan
add column keteranganRujukan varchar(255);

alter table rujukan
add column rsRujukan varchar(100);

-- rev 2
alter table pasien
add pemeriksa varchar(100);
alter table kode_obat
add column stok varchar(100);
alter table kode_obat
add column status_persediaan varchar(100);


---------------

--
-- Table structure for table `biaya_dokter`
--

CREATE TABLE `biaya_dokter` (
  `id` int(11) NOT NULL,
  `id_pasien` int(150) NOT NULL,
  `id_dokter` int(150) NOT NULL,
  `harga` varchar(10000) NOT NULL,
  `nama_dokter` varchar(225) NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8;

ALTER TABLE `biaya_dokter`
  ADD PRIMARY KEY (`id`);

ALTER TABLE `biaya_dokter`
  MODIFY `id` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=11;

--
-- Table structure for table `biaya_obat`
--

CREATE TABLE `biaya_obat` (
  `id` int(11) NOT NULL,
  `id_pasien` int(11) NOT NULL,
  `kode_obat` varchar(10000) NOT NULL,
  `jumlah` int(50) NOT NULL,
  `harga_total` varchar(10000) NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8;

ALTER TABLE `biaya_obat`
  ADD PRIMARY KEY (`id`);

ALTER TABLE `biaya_obat`
  MODIFY `id` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=11;


-- rev 3
alter table pasien
add column pendidikan varchar(100);
alter table pasien
add column kawin varchar(20);
ALTER TABLE pasien
MODIFY no_KTP VARCHAR(100) DEFAULT NULL;
